require_relative '../rgb'

describe "rgb_test" do
    HEX_1 = "#000000"
    HEX_2 = "#ffffff"
    HEX_3 = "#043c78"
    INITS_1 = [0, 0, 0]
    INITS_2 = [255, 255, 255]
    INITS_3 = [4, 60, 120]

    before do
        # 一々インミュータブルにするのがめんどくさい。後でインミュータブルで定数を宣言できるかググる。
        # ググったけどいい感じを方法がなさそう？
        HEX_1.freeze
        HEX_2.freeze
        HEX_3.freeze
        INITS_1.freeze
        INITS_2.freeze
        INITS_3.freeze
    end
    
    context "10進数から16進数への変換" do
        it "引数の10進数を16進数にして返却" do
            expect(to_hex(*INITS_1)).to eq(HEX_1)
            expect(to_hex(*INITS_2)).to eq(HEX_2)    
            expect(to_hex(*INITS_3)).to eq(HEX_3) 
        end
    end

    context "16進数から10進数への変換" do
        it "引数の16進数を10進数にして返却" do
            expect(to_inits(HEX_1)).to eq(INITS_1)
            expect(to_inits(HEX_2)).to eq(INITS_2) 
            expect(to_inits(HEX_3)).to eq(INITS_3)  
        end 
    end
end
