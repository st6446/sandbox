require_relative '../fizz_buzz'

describe "fizz_buzz" do
    FIZZ_BUZZ = "Fizz Buzz"
    BUZZ = "Buzz"
    FIZZ = "Fizz"
    # インミュータブルにしたい時はfreezeを使ったほうがいいらしい
    # https://docs.ruby-lang.org/ja/2.2.0/method/Object/i/freeze.html
    FIZZ_BUZZ.freeze
    BUZZ.freeze
    FIZZ.freeze
    
    context "3もしくは5の倍数ではない場合" do
        it "引数の数字が返却される" do
            expect(fizz_buzz(1)).to eq('1')
            expect(fizz_buzz(2)).to eq('2')
            expect(fizz_buzz(4)).to eq('4') 
        end 
    end

    context "3の倍数かつ5の倍数の場合" do
        it "#{FIZZ_BUZZ}が返却される" do
            expect(fizz_buzz(15)).to eq(FIZZ_BUZZ)
            expect(fizz_buzz(30)).to eq(FIZZ_BUZZ)
            expect(fizz_buzz(45)).to eq(FIZZ_BUZZ) 
        end      
    end

    context "3の倍数かつ5の倍数ではない場合" do
        it "#{FIZZ}が返却される" do
            expect(fizz_buzz(3)).to eq(FIZZ)
            expect(fizz_buzz(6)).to eq(FIZZ)
            expect(fizz_buzz(9)).to eq(FIZZ) 
        end 
    end

    context "3の倍数ではないかつ5の倍数の倍数" do
        it "#{BUZZ}が返却される" do
            expect(fizz_buzz(5)).to eq(BUZZ)
            expect(fizz_buzz(10)).to eq(BUZZ)
            expect(fizz_buzz(20)).to eq(BUZZ) 
        end     
    end
    
end
